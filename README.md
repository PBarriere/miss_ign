%------------------------------%
%--Protocole installation BDD--%
%------------------------------%

%Le php%
1-Ouvrez le dossier projet Web
2-Allez dans le dossier 'php'
3-Ouvrez le fichier 'connect.php'
4-Ouvrez MAMP
5-Avoir Le serveur Apache et MySQL qui fonctionne
6-Recuperez les informations de connection ('user','password','port'...)
7-Remplir aux endroits correspondants dans le fichier 'connect.php'

%La base de donnees%
8-Demandez a MAMP d'ouvrir sa page Web
9-Cliquez sur le lien PhpAdmin
10-Creez une base de donnees vide avec comme nom 'bdd' (important !)
11-Cliquez sur Import dans cette base de donnees
12-Le fichier a importe ce trouve dans le dossier assets du dossier projet Web et est nomme 'BDD.sql'
13-Importez-le et cliquer sur GO en bas de page

La BDD est installee!

%-------%
%--Jeu--%
%-------%

1-MAMP doit etre lance avec le serveur Apache et MySQL
2-Le dossier projet Web doit faire parti du dossier que lance MAMP, si ce n'est pas le cas, changez les preferences pour choisir le bon dossier
3-Sur un navigateur Web, tapez 'localhost' dans sa barre de navigation
4-ouvrez le fichier 'index.html' du dossier Web

Le jeu se lance!

%-----------%
%--Contenu--%
%-----------%

Vous trouverez dans le menu le meilleur joueur,si il y en a un, qui defile, le bouteau 'nouveau jeu' pour jouer et 'meilleurs resultats' pour acceder aux tableau des temps.

*Tableau des temps* - On peut les ranger par rang ou nom et trouver un joueur a partir de son pseudo

*Jeu* 1-Pour lire un objet cliquer une fois
	  2-Pour le prendre ou activer son enigme il faut double cliquer.
	  3-Dans l'inventaire, on peut acceder a la description d'un objet en cliquant dessus, si l'on reclique l'image revient
	  4-Pour acceder a un objet qui demande des objets de l'inventaire, il faut cocher les objets correspondants dans l'inventaire avant de double cliquer.
	  5-Il y a une gestion des zooms, pensez a bien zoomer pour trouver certains objets.
	  6-Les enigmes sont validees en cliquant sur le bouton 'valider', il est possible de fermer une enigme et de la reouvrir ulterieurement.
	  6-Si jamais un probleme d'affichage a lieu, recharger la page recommencera le jeu depuis le debut et reinitialisera le chrono.
	  
*La fin* - Une fois fini il est possible d'enregistrer votre temps et pseudo, une fois la mention 'Ajoute!' apparue, le joueur et son temps sont enregistres dans votre bdd.
(Vous pourrez donc le retrouvez meme si vous fermez votre navigateur tant que vous ne faites pas de modification via php Admin)

Merci pour avoir pris le temps de lire ces indications.
ReadME redige par Pierre-Marie Demaie et Paul Barriere.





