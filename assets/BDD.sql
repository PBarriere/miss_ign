-- phpMyAdmin SQL Dump
-- version 2.11.8.1deb5+lenny3
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Version du serveur: 5.0.51
-- Version de PHP: 5.2.6-1+lenny4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `BDD`
--

-- --------------------------------------------------------

--
-- Structure de la table `objets`
--

CREATE TABLE IF NOT EXISTS `tbl_objets` (
  `id` int(11) NOT NULL,
  `nom` varchar(20) collate utf8_unicode_ci NOT NULL,
  `type` varchar(20) collate utf8_unicode_ci NOT NULL,
  `indice` varchar(500) collate utf8_unicode_ci NOT NULL,
  `soluce` varchar(500) collate utf8_unicode_ci NOT NULL,
  `resolu` varchar(500) collate utf8_unicode_ci NOT NULL,
  `lat` double,
  `long` double,
  `zoom` int(11),
  `bloque` int(11),
  `libere` int(11),
  `icone` varchar(500) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `tbl_objets`
--
INSERT INTO `tbl_objets`(`id`, `nom`, `type`,`indice`,`soluce`, `resolu`, `lat`, `long`, `zoom`, `bloque`, `libere`, `icone`) VALUES
(1,"Opéra Garnier","indication","Le chef d'orchestre est le dernier à l'avoir vue:' Elle s'est probablement barrée à l'autre bout du monde et nous fait toute une scène dramatique! Les opéras donnent envie de visiter le monde. Tenez prenez ses partitions... A tous les coups, elle aura voulu s'imprégner du personnage', Appelez les aéroports correspondants et revenez me voir!'",NULL, NULL,48.871953,2.331805,18,NULL,2,"../images/masque.png"),
(2,"Opéra Garnier", "quete", "'Alors ces coup de fils?'",NULL,NULL,48.871953,2.331805,5,1,15,"../images/question.png"),
(3,"Nagasaki","objets","Nagasaki?",NULL,"Oui l'action a bien lieu au Japon! Mais pas de nouvelles d'Inès.",36.315125,138.62011,1,1,2,"../images/phone.png"),
(4,"Paris","objets","Paris?",NULL,"Ville parfaite pour la romance! Mais pas de trace de votre diva.",48.856612,2.351347,1,1,2,"../images/phone.png"),
(5,"Espagne","objets","Espagne?",NULL,"Oui, le créateur de la pièce originale est français, le compositeur est Autrichien (Mozart), Mais tout se passe en Espagne...Comment? Ha non on l'a pas vu",39.909736,-3.94537,1,1,2,"../images/phone.png"),
(6,"Celtes","objets","Gaulle?",NULL,"Nous sommes en 50 av JC, toute la Gaulle est occupée par les romains...Mais toujours aucune trace de la cantatrice",47.872144,-3.94537,1,1,2,"../images/phone.png"),
(7,"Nouveau Monde","objets","USA?",NULL,"Faut le savoir! sinon RAS.",39.368279,-77.265478,1,1,2,"../images/phone.png"),
(8,"Lieu biblique","objets","Babylon?",NULL,"La Bible, dans l'ancien testament, est plutôt violente. Mais ce ne semble pas être la direction qu'à pris Anès.",31.728167,39.953526,1,1,2,"../images/phone.png"),
(9,"Madama Butterfly","inv","Madama Butterfly de Puccini",NULL,"Madama Butterfly de Puccini",1,1,1,1,3,"../images/note.png"),
(10,"La Traviata","inv","La traviata de Verdi",NULL,"La traviata de Verdi",1,1,1,1,4,"../images/note.png"),
(11,"Le Mariage de Figaro","inv","Le Mariage de Figaro de Mozart",NULL,"Le Mariage de Figaro de Mozart",1,1,1,1,5,"../images/note.png"),
(12,"Norma","inv","Norma de Bellini",NULL,"Norma de Bellini",1,1,1,1,6,"../images/note.png"),
(13,"Le sacre du Printemps","inv","Le sacre du Printemps de Stravinsky ",NULL,"Le sacre du Printemps de Stravinsky",1,1,1,1,7,"../images/note.png"),
(14,"Nabuchodonosor","inv","Nabuchodonosor de Verdi",NULL,"Nabuchodonosor de Verdi",1,1,1,1,8,"../images/note.png"),
(15,"Opéra Garnier","objets","Ha bon! Pas de nouvelles? J'en ai parlé avec les musiciens, la dernière fois qu'ils l'ont vue, elle sortait de l'opéra avec le ténor Francezzo, ils ont fait tomber ça, ça pourra peut-être vous aider.",NULL,"porte-clés",48.871953,2.331805,1,2,16,"../images/paris.png"),
(16,"Tour Eiffel","objets","Francezzo est seul sur un banc. 'Comment? Oui nous sommes sortis pour admirer la vue depuis la Tour, mais je l'ai quitté des yeux le temps de prendre les billets et elle avait disparu! Avant sa disparition elle a mentionné un cadeau d'Eiffel pour Sam, ça vous dit quelque chose?",NULL,"Un cadeau fait par Eiffel pour Sam?",48.858225,2.294506,16,15,17,"../images/pensee.png"),
(17,"Statue de la liberté","objets","Oui c'est ça, la Statue de la Liberté! Mais sur Paris, il y a un cadeau similaire d'une nation étrangère, il me semble qu'elle aimait beaucoup cet endroit...'",NULL,"Un cadeau sur Paris d'une nation étrangère",40.689242,-74.044556,17,16,18,"../images/pensee.png"),
(18,"Obélisque","enigme","Un vendeur de journaux vous prend à partie,'Il y avait une dame qui ressemble à votre description. Hier soir elle s'est faite aborder par un étrange monsieur, il m'a dit de donner ça à celui qui résout cette énigme: Ramsès dit à Isis:'J'ai 4 fois l'âge que tu avais lorsque j'avais l'âge que tu as maintenant'.Ramses a 40 ans, quel âge a Isis? (Pour 1 ans entrez 1)",25,"C'est ça, voici l'objet qu'il m'a donné. Je n'ai pas bien vu mais il a fait entrer la cantatrice de façon un peu menaçante dans une voiture noire. Bonne chance pour la suite !' Il vous tend un livre:'Codes et Enigmes du Monde' Emprunté à la BNF",48.865481, 2.321125,17,17,NULL,"../images/question.png"),
(19,"Livre","inv","'Codes et Enigmes du Monde' Emprunté à la BNF",NULL,"'Codes et Enigmes du Monde' Emprunté à la BNF",1,1,1,18,20,"../images/livre.png"),
(20," entrée de la BNF","enigme","En arrivant sur place, le bibliothécaire fait signe de se taire: 'Chut! Comment? Vous voulez savoir qui a emprunté ce livre ? J'ai autre chose à faire...Vous pouvez m'aider d'ailleurs: 'Ohv mhxa vrqw idlwv' je cherche le sens de ces mots, une des clés de lecture est 'Le Grand Cirque à Rome'. Regardez cette carte et aidez moi à déchiffrer!","Les jeux sont faits","Merci! Vous allez l'air doué!! Attendez moi ici je vais chercher dans les archives.",48.833778, 2.375622,16,19,23,"../images/question.png"),
(21,"Code César","code","Code César avec décalage de 3",NULL,"Code César avec décalage de 3",41.890218, 12.492356,16,NULL,NULL,"../images/colisee.jpg"),
(23,"cadena","enigme","Oh Non! Les archives sont bloquées par un code à 4 chiffres! Je ne m'en souviens plus...Je crois qu'un mémo se trouve dans un livre de la bibliothèque, c'est un livre sur une Dame très connue de Paris, ça vous dit quelque chose?",7335,"Parfait",48.833778, 2.375622,1,20,24,"../images/cadena.png"),
(22,"Code étrange (annoté d'une référence:'The Ciphers of the Monks: a Forgotten Number-notation of the Middle Ages')","code","Code étrange (annoté d'une référence:'The Ciphers of the Monks: a Forgotten Number-notation of the Middle Ages')",NULL,NULL,48.852934, 2.350055,18,NULL,NULL,"../images/code2.png"),
(24,"livre","objets","'Il s'avère que la personne qui a emprunté ce livre en a emprunté un autre: 'De la Fronde à la Gloire' un ouvrage historique...C'est peut-être un indice pour le retrouver?",NULL," 'De la Fronde à la Gloire' ouvrage historique",48.833778, 2.375622,1,23,25,"../images/livre.png"),
(25,"Final","enigme","Vous arrivez sur place, un homme se trouve en face de vous:'Enfin quelqu'un qui m'a trouvé. Si j'ai enlevé Inès, c'est pour empêcher des prétendants idiots de s'en emparer, ils ne sont pas assez bien pour elle! J'accepterai de la libérer si tu résous cette énigme:'Qu'est-ce qui est meilleur que dieu, pire que le diable, les pauvres en ont, les riches en ont besoin et si on en mange, on meurt?' Alors ? En un mot ? (Commençant par une majuscule)","Rien","Je m'avoue vaincu, prends cette clé, c'est celle du Petit Trianon.",48.804744, 2.120437,16,24,26,"../images/question.png"),
(26,"clé","objets","clé du Petit Trianon",NULL,"clé du Petit Trianon",48.804744, 2.120437,16,25,27,"../images/cle.png"),
(27,"Inès","end","Libérer Inès",NULL,"Libérer Inès",48.815586, 2.109719,18,26,NULL,"../images/cible.png");

CREATE TABLE IF NOT EXISTS `tbl_players` (
  `id_player` int not NULL AUTO_INCREMENT,
  `rang` int(11) NOT NULL,
  `nom` varchar(20) collate utf8_unicode_ci NOT NULL,
  `temps` varchar(20) collate utf8_unicode_ci,
  PRIMARY KEY(`id_player`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;





