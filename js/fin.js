/*
-------------------------------------------------------------------------
------------------------------Fonctions----------------------------------
-------------------------------------------------------------------------
*/
function get() {
    /*
    Fonction permettant de récupérer le temps du joueur
    :return: temps du joueur
    :rtype: string
    */
    var temps = sessionStorage.getItem("temps");
    return temps;
}


function addPlayer(nom, temps) {
    /*
    Fonction qui permet d'enregistrer le joueur et son temps dans la base de données
    :param nom: pseudo choisi par le joueur
    :type nom: string
    :param temps: temps du joueur
    :param temps: string
    */
    var data = "add_player=" + nom + "&temps=" + temps;
    fetch("../php/bdd.php", {
        method: "post",
        body: data,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        }
    });
}

function valider(event) {
    /*
    Fonction permettant de valider l'entrée du pseudo afin qu'il soit conforme à la BDD
    */
    var champ_nom = pseudo.value;
    var form_OK = true;
    if (champ_nom == "") {
        form_OK = false;
        pseudo.classList.add("erreur");
    }
    if (form_OK) {
        addPlayer(champ_nom, temps);
        pseudo.classList.remove("erreur");
        indic.innerHTML = "Ajouté!";
    } else {
        pseudo.value = "";
        indic.innerHTML = "Veuillez réessayer";
    }
}


/*
----------------------------------------------------------
--------------Récupération données document---------------
----------------------------------------------------------
*/
//temps
var temps = get();
var tempo = document.getElementById("tempo");
tempo.innerHTML = temps;

//initialisation du pseudo
var pseudo = document.getElementById("pseudo");
pseudo.value = "";

//bouton s'enregistrer
var enregistrement = document.getElementById("enregistrer");
enregistrement.addEventListener('click', valider);

//indication en cas d'erreur
var indic = document.getElementById("indication");