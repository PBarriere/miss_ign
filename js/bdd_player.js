/*
---------------------------------------------------
---------Gestion du Tableau des résultats----------
---------------------------------------------------
*/

//récupération des éléments HTML 
var tab = document.getElementById("content");
var b_rang = document.getElementById("rang");
var b_noms = document.getElementById("noms");
var text = document.getElementById("input");

//Ajout EventListener
window.addEventListener("load", generateTabByRank);
b_rang.addEventListener("click", generateTabByRank);
b_noms.addEventListener("click", generateTabByNames);
text.addEventListener("input", search)

function search() {
    /*
    Fonction qui permet à un utilisateur de chercher un joueur dans la base de données
    */
    var data = "tb_player=" + text.value;
    fetch("../php/bdd.php", {
            method: "post",
            body: data,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }

        })
        .then(r => r.text())
        .then(r => {
            tab.innerHTML = r;
        })
}

function generateTabByRank() {
    /*
    Fonction qui permet l'affichage des joueurs par rang
    */
    var data = "tb_player=rank";
    fetch("../php/bdd.php", {
            method: "post",
            body: data,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }

        })
        .then(r => r.text())
        .then(r => {
            tab.innerHTML = r;
        })
}

function generateTabByNames() {
    /*
    Fonction qui permet l'affichage des joueur par nom
    */
    var data = "tb_player=names";
    fetch("../php/bdd.php", {
            method: "post",
            body: data,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }

        })
        .then(r => r.text())
        .then(r => {
            tab.innerHTML = r;
        })
}