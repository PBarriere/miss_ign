/*
----------------------------------------------------------------------------
----------------------------GESTION DE L'INTERFACE--------------------------
----------------------------------------------------------------------------
*/

//---Chronomètre---
//Paramètres
var s = 0, //seconde
    mn = 0, //minute
    h = 0; //heure
var t = 0; //chrono
var chrono = document.getElementById("chrono");

//Fonctions
function start() {
    /*
    Fonction démarant le chrono
    */
    reset();
    t = setInterval(majChrono, 1000);
}

function affichage(h, mn, s) {
    /*
    Fonction affichant le chrono correctement
    :param h: heures
    :type h: int
    :param mn: minutes
    :type mn: int
    :param s: secondes
    :type s: int
    */
    var sec = "0";
    var min = "0";
    var heu = "0";
    if (Math.floor(s / 10) == 0) { //mettre le 0 devant
        sec = sec + s;
    } else {
        sec = s;
    }
    if (Math.floor(mn / 10) == 0) { //mettre le 0 devant
        min = min + mn;
    } else {
        min = mn;
    }
    if (Math.floor(h / 10) == 0) { //mettre le 0 devant
        heu = heu + h;
    } else {
        heu = h;
    }
    var temps = heu + ":" + min + ":" + sec;
    chrono.innerHTML = temps;
}

function majChrono() {
    /*
    Fonction mettant à jour le chrono
    */
    s += 1;
    if (s == 60) { //passage d'une minute
        s = 0;
        mn += 1;
    }
    if (mn == 60) { //passage d'une heure
        mn = 0;
        h += 1;
    }
    affichage(h, mn, s);
}

function stop() {
    /*
    Fonction mettant fin au chrono et sauvegardant le temps
    */
    clearInterval(t); //fin chrono
    var temps = chrono.textContent;
    sessionStorage.setItem("temps", temps); //sauvegarde
    location.href = "./fin.html"; //fin jeu
}

function reset() {
    /*
    Fonction remettant le chrono à 0
    */
    clearInterval(t);
    s = 0, mn = 0, h = 0;
    affichage(h, mn, s);
}

//---Inventaire---

//paramètres
var textexplic = [indice1, indice2, indice3, indice4, indice5, indice6, indice7, indice8, indice9, indice10];
var textretour = [retour1, retour2, retour3, retour4, retour5, retour6, retour7, retour8, retour9, retour10]; //fonction inventaire
var checkfunc = [compter1, compter2, compter3, compter4, compter5, compter6, compter7, compter8, compter9, compter10]; //fonction sélection
var checkboxes = ["checkbox1", "checkbox2", "checkbox3", "checkbox4", "checkbox5", "checkbox6", "checkbox7", "checkbox8", "checkbox9", "checkbox10"]; //éléments inventaires

//fonctions
function get_emplacement() {
    /*
    Fonction récupérant la place de l'inventaire vide la plus proche de 0
    */
    var i = 1;
    while (i < 11 && document.getElementById("el_" + i).textContent != "") {
        i++;
    }
    if (i < 11) {
        return i; //emplacement trouvé
    } else {
        return ""; //sinon
    }
}

function indice1() {
    /*
    Fonction indice permettant de passer de l'icône d'un objet à son texte explicatif
    */
    var targethtml = document.getElementById('cible1');
    var id = Number(document.getElementById("el_1").textContent);
    var ind = document.getElementById("el_resolu" + id).textContent;
    targethtml.innerHTML = "<textarea>" + ind + "</textarea>";
    targethtml.removeEventListener('click', indice1);
    targethtml.addEventListener('click', retour1);

}

function retour1() {
    /*
    Fonction retour permettant de passer du texte explicatif d'un objet à son icône
    */
    var targethtml = document.getElementById('cible1');
    var id = Number(document.getElementById("el_1").textContent);
    var ico = document.getElementById("el_icone" + id).textContent;
    targethtml.innerHTML = "<input type='image' src='" + ico + "' width='100%' height='100%'></input>";
    targethtml.removeEventListener('click', retour1);
    targethtml.addEventListener('click', indice1);
}

function compter1() {
    /*
    Fonction compter permettant de sélectionner ou déselectionner un objet présent dans l'inventaire
    */
    var num = document.getElementById("el_1").textContent;
    num = Number(num);
    if (checkbox1.checked) {
        selection.push(num);
    } else {
        selection = supprimer(selection, num);
    }


}

function indice2() {
    /*
    Fonction indice permettant de passer de l'icône d'un objet à son texte explicatif
    */
    var targethtml = document.getElementById('cible2');
    var id = Number(document.getElementById("el_2").textContent);
    var ind = document.getElementById("el_resolu" + id).textContent;
    targethtml.innerHTML = "<textarea>" + ind + "</textarea>";
    targethtml.removeEventListener('click', indice2);
    targethtml.addEventListener('click', retour2);

}

function retour2() {
    /*
    Fonction retour permettant de passer du texte explicatif d'un objet à son icône
    */
    var targethtml = document.getElementById('cible2');
    var id = Number(document.getElementById("el_2").textContent);
    var ico = document.getElementById("el_icone" + id).textContent;
    targethtml.innerHTML = "<input type='image' src='" + ico + "' width='100%' height='100%'></input>";
    targethtml.removeEventListener('click', retour2);
    targethtml.addEventListener('click', indice2);
}

function compter2() {
    /*
    Fonction compter permettant de sélectionner ou déselectionner un objet présent dans l'inventaire
    */
    var num = document.getElementById("el_2").textContent;
    num = Number(num);
    if (checkbox2.checked) {
        selection.push(num);
    } else {
        selection = supprimer(selection, num);
    }
}

function indice3() {
    /*
    Fonction indice permettant de passer de l'icône d'un objet à son texte explicatif
    */
    var targethtml = document.getElementById('cible3');
    var id = Number(document.getElementById("el_3").textContent);
    var ind = document.getElementById("el_resolu" + id).textContent;
    targethtml.innerHTML = "<textarea>" + ind + "</textarea>";
    targethtml.removeEventListener('click', indice3);
    targethtml.addEventListener('click', retour3);
}

function retour3() {
    /*
    Fonction retour permettant de passer du texte explicatif d'un objet à son icône
    */
    var targethtml = document.getElementById('cible3');
    var id = Number(document.getElementById("el_3").textContent);
    var ico = document.getElementById("el_icone" + id).textContent;
    targethtml.innerHTML = "<input type='image' src='" + ico + "' width='100%' height='100%'></input>";
    targethtml.removeEventListener('click', retour3);
    targethtml.addEventListener('click', indice3);
}

function compter3() {
    /*
    Fonction compter permettant de sélectionner ou déselectionner un objet présent dans l'inventaire
    */
    var num = document.getElementById("el_3").textContent;
    num = Number(num);
    if (checkbox3.checked) {
        selection.push(num);
    } else {
        selection = supprimer(selection, num);
    }
}

function indice4() {
    /*
    Fonction indice permettant de passer de l'icône d'un objet à son texte explicatif
    */
    var targethtml = document.getElementById('cible4');
    var id = Number(document.getElementById("el_4").textContent);
    var ind = document.getElementById("el_resolu" + id).textContent;
    targethtml.innerHTML = "<textarea>" + ind + "</textarea>";
    targethtml.removeEventListener('click', indice4);
    targethtml.addEventListener('click', retour4);
}

function retour4() {
    /*
    Fonction retour permettant de passer du texte explicatif d'un objet à son icône
    */
    var targethtml = document.getElementById('cible4');
    var id = Number(document.getElementById("el_4").textContent);
    var ico = document.getElementById("el_icone" + id).textContent;
    targethtml.innerHTML = "<input type='image' src='" + ico + "' width='100%' height='100%'></input>";
    targethtml.removeEventListener('click', retour4);
    targethtml.addEventListener('click', indice4);
}

function compter4() {
    /*
    Fonction compter permettant de sélectionner ou déselectionner un objet présent dans l'inventaire
    */
    var num = document.getElementById("el_4").textContent;
    num = Number(num);
    if (checkbox4.checked) {
        selection.push(num);
    } else {
        selection = supprimer(selection, num);
    }
}

function indice5() {
    /*
    Fonction indice permettant de passer de l'icône d'un objet à son texte explicatif
    */
    var targethtml = document.getElementById('cible5');
    var id = Number(document.getElementById("el_5").textContent);
    var ind = document.getElementById("el_resolu" + id).textContent;
    targethtml.innerHTML = "<textarea>" + ind + "</textarea>";
    targethtml.removeEventListener('click', indice5);
    targethtml.addEventListener('click', retour5);
}

function retour5() {
    /*
    Fonction retour permettant de passer du texte explicatif d'un objet à son icône
    */
    var targethtml = document.getElementById('cible5');
    var id = Number(document.getElementById("el_5").textContent);
    var ico = document.getElementById("el_icone" + id).textContent;
    targethtml.innerHTML = "<input type='image' src='" + ico + "' width='100%' height='100%'></input>";
    targethtml.removeEventListener('click', retour5);
    targethtml.addEventListener('click', indice5);
}

function compter5() {
    /*
    Fonction compter permettant de sélectionner ou déselectionner un objet présent dans l'inventaire
    */
    var num = document.getElementById("el_5").textContent;
    num = Number(num);
    if (checkbox5.checked) {
        selection.push(num);
    } else {
        selection = supprimer(selection, num);
    }
}

function indice6() {
    /*
    Fonction indice permettant de passer de l'icône d'un objet à son texte explicatif
    */
    var targethtml = document.getElementById('cible6');
    var id = Number(document.getElementById("el_6").textContent);
    var ind = document.getElementById("el_resolu" + id).textContent;
    targethtml.innerHTML = "<textarea>" + ind + "</textarea>";
    targethtml.removeEventListener('click', indice6);
    targethtml.addEventListener('click', retour6);
}

function retour6() {
    /*
    Fonction retour permettant de passer du texte explicatif d'un objet à son icône
    */
    var targethtml = document.getElementById('cible6');
    var id = Number(document.getElementById("el_6").textContent);
    var ico = document.getElementById("el_icone" + id).textContent;
    targethtml.innerHTML = "<input type='image' src='" + ico + "' width='100%' height='100%'></input>";
    targethtml.removeEventListener('click', retour6);
    targethtml.addEventListener('click', indice6);
}

function compter6() {
    /*
    Fonction compter permettant de sélectionner ou déselectionner un objet présent dans l'inventaire
    */
    var num = document.getElementById("el_6").textContent;
    num = Number(num);
    if (checkbox6.checked) {
        selection.push(num);
    } else {
        selection = supprimer(selection, num);
    }
}

function indice7() {
    /*
    Fonction indice permettant de passer de l'icône d'un objet à son texte explicatif
    */
    var targethtml = document.getElementById('cible7');
    var id = Number(document.getElementById("el_7").textContent);
    var ind = document.getElementById("el_resolu" + id).textContent;
    targethtml.innerHTML = "<textarea>" + ind + "</textarea>";
    targethtml.removeEventListener('click', indice7);
    targethtml.addEventListener('click', retour7);
}

function retour7() {
    /*
    Fonction retour permettant de passer du texte explicatif d'un objet à son icône
    */
    var targethtml = document.getElementById('cible7');
    var id = Number(document.getElementById("el_7").textContent);
    var ico = document.getElementById("el_icone" + id).textContent;
    targethtml.innerHTML = "<input type='image' src='" + ico + "' width='100%' height='100%'></input>";
    targethtml.removeEventListener('click', retour7);
    targethtml.addEventListener('click', indice7);

}

function compter7() {
    /*
    Fonction compter permettant de sélectionner ou déselectionner un objet présent dans l'inventaire
    */
    var num = document.getElementById("el_7").textContent;
    num = Number(num);
    if (checkbox7.checked) {
        selection.push(num);
    } else {
        selection = supprimer(selection, num);
    }
}

function indice8() {
    /*
    Fonction indice permettant de passer de l'icône d'un objet à son texte explicatif
    */
    var targethtml = document.getElementById('cible8');
    var id = Number(document.getElementById("el_8").textContent);
    var ind = document.getElementById("el_resolu" + id).textContent;
    targethtml.innerHTML = "<textarea>" + ind + "</textarea>";
    targethtml.removeEventListener('click', indice8);
    targethtml.addEventListener('click', retour8);
}

function retour8() {
    /*
    Fonction retour permettant de passer du texte explicatif d'un objet à son icône
    */
    var targethtml = document.getElementById('cible8');
    var id = Number(document.getElementById("el_8").textContent);
    var ico = document.getElementById("el_icone" + id).textContent;
    targethtml.innerHTML = "<input type='image' src='" + ico + "' width='100%' height='100%'></input>";
    targethtml.removeEventListener('click', retour8);
    targethtml.addEventListener('click', indice8);
}

function compter8() {
    /*
    Fonction compter permettant de sélectionner ou déselectionner un objet présent dans l'inventaire
    */
    var num = document.getElementById("el_8").textContent;
    num = Number(num);
    if (checkbox8.checked) {
        selection.push(num);
    } else {
        selection = supprimer(selection, num);
    }
}

function indice9() {
    /*
    Fonction indice permettant de passer de l'icône d'un objet à son texte explicatif
    */
    var targethtml = document.getElementById('cible9');
    var id = Number(document.getElementById("el_9").textContent);
    var ind = document.getElementById("el_resolu" + id).textContent;
    targethtml.innerHTML = "<textarea>" + ind + "</textarea>";
    targethtml.removeEventListener('click', indice9);
    targethtml.addEventListener('click', retour9);
}

function retour9() {
    /*
    Fonction retour permettant de passer du texte explicatif d'un objet à son icône
    */
    var targethtml = document.getElementById('cible9');
    var id = Number(document.getElementById("el_9").textContent);
    var ico = document.getElementById("el_icone" + id).textContent;
    targethtml.innerHTML = "<input type='image' src='" + ico + "' width='100%' height='100%'></input>";
    targethtml.removeEventListener('click', retour9);
    targethtml.addEventListener('click', indice9);
}

function compter9() {
    /*
    Fonction compter permettant de sélectionner ou déselectionner un objet présent dans l'inventaire
    */
    var num = document.getElementById("el_9").textContent;
    num = Number(num);
    if (checkbox9.checked) {
        selection.push(num);
    } else {
        selection = supprimer(selection, num);
    }
}

function indice10() {
    /*
    Fonction indice permettant de passer de l'icône d'un objet à son texte explicatif
    */
    var targethtml = document.getElementById('cible10');
    var id = Number(document.getElementById("el_10").textContent);
    var ind = document.getElementById("el_resolu" + id).textContent;
    targethtml.innerHTML = "<textarea>" + ind + "</textarea>";
    targethtml.removeEventListener('click', indice10);
    targethtml.addEventListener('click', retour10);
}

function retour10() {
    /*
    Fonction retour permettant de passer du texte explicatif d'un objet à son icône
    */
    var targethtml = document.getElementById('cible10');
    var id = Number(document.getElementById("el_10").textContent);
    var ico = document.getElementById("el_icone" + id).textContent;
    targethtml.innerHTML = "<input type='image' src='" + ico + "' width='100%' height='100%'></input>";
    targethtml.removeEventListener('click', retour10);
    targethtml.addEventListener('click', indice10);
}

function compter10() {
    /*
    Fonction compter permettant de sélectionner ou déselectionner un objet présent dans l'inventaire
    */
    var num = document.getElementById("el_10").textContent;
    num = Number(num);
    if (checkbox10.checked) {
        selection.push(num);
    } else {
        selection = supprimer(selection, num);
    }
}


//---markers et affichage---
function get_objet(mark) {
    /*
    Fonction qui supprime un objet de la carte pour le placer dans l'inventaire et déclencher la boucle icone/texte explicatif et la possibilité de le sélectionner.
    :param mark: marker
    :type mark: marker
    */
    var emp = get_emplacement(); //récupérer emplacement
    if (emp != "") { //sécurité
        mymap.removeLayer(mark); //enlever marker
        var targethtml = document.getElementById('cible' + emp);
        var ico = document.getElementById("el_icone" + mark.id).textContent;
        var typ = document.getElementById("el_type" + mark.id).textContent;
        if (typ == "objets" || typ == "inv") { //comportement habituel
            targethtml.innerHTML = "<input type='image' src='" + ico + "' width='100%' height='100%'></input>";
            document.getElementById("check" + emp).innerHTML = "<input type='checkbox' id='checkbox" + emp + "'></input>";
            document.getElementById("checkbox" + emp).addEventListener('change', checkfunc[emp - 1]);
            document.getElementById("el_" + emp).innerHTML = mark.id;
            targethtml.addEventListener('click', textexplic[emp - 1]);
        } else {
            selection.push(mark.id);
        }
    }
}

function getMark(id) {
    /*
    Fonction qui récupère la variable marker correspondant à son id
    :param id: id correspondant à l'objet
    :type id: int
    :return: marker associé
    :rtype: marker
    */
    for (var i = 0; i < markers.length; i++) {
        if (Number(markers[i][0].id) == id) {
            var r = markers[i][0];
        }
    }
    return r;
}

function supprimer(tabselection, id) {
    /*
    Fonction supprimant l' élément id d'un tableau
    :param tabselection: tableau des éléments
    :type tabselection: list int
    :param id: id de l'élément à supprimer
    :type id: int
    :return: tableau avec élément
    :rtype: list int
    */
    var tab = []
    tabselection.forEach(function(element) {
        if (element != id) { tab.push(element); }
    })
    return tab;
}

function create_mark(i) {
    /*
    Fonction créant le marker sur la carte à partir de son id
    :param i: id de l'objet
    :type i: int
    */
    var long = Number(document.getElementById("el_long" + i).textContent);
    var lat = Number(document.getElementById("el_lat" + i).textContent);
    var ico = document.getElementById("el_icone" + i).textContent;
    var ind = document.getElementById("el_indice" + i).textContent;
    var type = document.getElementById("el_type" + i).textContent;
    var zoom = Number(document.getElementById("el_zoom" + i).textContent);
    var nom = document.getElementById("el_nom" + i).textContent;
    var icone = L.icon({
        iconUrl: ico,
        iconSize: [38, 38], // size of the icon
    });
    if (type == "enigme") { //pour les énigmes
        var marker = L.marker([lat, long], { icon: icone }).bindPopup(nom).addTo(mymap).addEventListener("dblclick", function() { find(marker.id, selection); });
    } else { //objets
        if (type == "code") { //pour les énigmes
            var marker = L.marker([lat, long], { icon: icone }).bindPopup(ind).addTo(mymap);
        } else {
            var marker = L.marker([lat, long], { icon: icone }).bindPopup(ind).addTo(mymap).addEventListener("dblclick", function() { find(marker.id, selection); });
        }
    }
    marker.id = i;
    let a = [marker, ico, zoom];
    markers.push(a);
    majMark();
    if (type == "inv") {
        find(i, selection);
    }
    if (type == "indication") {
        max_indidication = i;
    }
}

function afficher(tab_el) {
    /*
    Fonction permettant d'afficher une liste d'élément selon lors catégories
    :param tab_el: liste des id à afficher
    :type tab_el: list int
    */
    var end = tab_el.length;
    for (var i = 0; i < end; i++) {
        create_mark(tab_el[i]);
    }

}

function majMark() {
    /*
    Fonction rendant visible ou invisible les markers selon le niveau de zoom
    */
    var currentZoom = mymap.getZoom();
    markers.forEach(el => {
        var icone = L.icon({ //visible
            iconUrl: el[1],
            iconSize: [38, 38], // size of the icon  
        });
        if (currentZoom < el[2]) { //invisible
            icone = L.icon({
                iconUrl: el[1],
                iconSize: [0, 0], // size of the icon  
            });
        }
        el[0].setIcon(icone);
    });
}

function utilisation(id) {
    /*
    Fonction utilisant les objets sélectionnés dans l'inventaire, ce qui résulte en leur suppression et l'obtention de l'objet débloqué
    :param id: id de l'objet concerné
    :type id: int
    */
    var obj_to_delete = selection;
    selection = [];
    var type = document.getElementById("el_type" + id).textContent;
    for (var i = 0; i < obj_to_delete.length; i++) {
        if (type != "quete" && id != obj_to_delete[i]) {
            if (obj_to_delete[i] > max_indidication) {
                supprimer_objet(obj_to_delete[i]);
            } else {
                selection.push(obj_to_delete[i]);
            }
        } else {
            supprimer_objet(obj_to_delete[i]);
            selection = supprimer(selection, Number(id));
        }
    }
}

function supprimer_objet(id) {
    /*
    Fonction supprimant de l'inventaire l'objet correspondant à id.
    :param id: id de l'objet à supprimer dans inventaire
    :type id: int
    */
    for (var i = 1; i < 11; i++) {
        var targethtml = document.getElementById('cible' + i);
        if (document.getElementById("el_" + i).textContent == id) {
            document.getElementById("el_" + i).textContent = '';
            document.getElementById('cible' + i).innerHTML = '';
            document.getElementById('check' + i).innerHTML = '';
            targethtml.removeEventListener('click', textexplic[i - 1]);
            targethtml.removeEventListener('click', textretour[i - 1]);
            break;
        }
    }
}

/*
----------------------------------------------------------------------------
----------------------------GESTION DES DONNEES-----------------------------
----------------------------------------------------------------------------
*/
//paramètres
var max_indidication = 1;

//---php---
function getById() {
    /*
    Fonction qui récupère les informations de l'objet courant, en les assignant à des variables de stockage
    :param id: id de l'objet
    :type id:int
    */
    var data = "id=all";
    fetch("../php/bdd.php", {
            method: "post",
            body: data,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }

        })
        .then(r => r.json())
        .then(r => {
            if (r != null) {
                for (var id = 1; id < 28; id++) {
                    var el_icone = document.getElementById("el_icone" + id);
                    var el_indice = document.getElementById("el_indice" + id);
                    var el_resolu = document.getElementById("el_resolu" + id);
                    var el_long = document.getElementById("el_long" + id);
                    var el_lat = document.getElementById("el_lat" + id);
                    var el_zoom = document.getElementById("el_zoom" + id);
                    var el_nom = document.getElementById("el_nom" + id);
                    var el_soluce = document.getElementById("el_soluce" + id);
                    var el_type = document.getElementById("el_type" + id);
                    var el_id = document.getElementById("el_id" + id);
                    el_icone.innerHTML = r[id]["icone"];
                    el_indice.innerHTML = r[id]["indice"];
                    el_resolu.innerHTML = r[id]["resolu"];
                    el_long.innerHTML = r[id]["long"];
                    el_lat.innerHTML = r[id]["lat"];
                    el_zoom.innerHTML = r[id]["zoom"];
                    el_nom.innerHTML = r[id]["nom"];
                    el_soluce.innerHTML = r[id]["soluce"];
                    el_type.innerHTML = r[id]["type"];
                    el_id.innerHTML = id;
                }
            }
        });

}

function find(id, tb_ele) {
    /*
    Fonction find permet de vérifier si l'on dévérouille un objet que l'on a trouvé sur la carte
    :param id: objet que l'on a trouvé sur la carte
    :type id: int
    :param tb_ele: liste des id des objets que l'on a dans son inventaire
    :type tb_ele: list int
    :return: list des id libérés par l'objet pour être affichés (par free)
    :rtype: array tableau associatif avec comme clé id de l'objet
    */
    var data = "tb_lock=" + id;
    //Récupération des objets qui le bloquent
    fetch("../php/bdd.php", {
            method: "post",
            body: data,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }

        })
        .then(r => r.json())
        .then(r => {
            //vérifie que l'on est dans notre inventaire tous les objets pour libérer l'objet
            var i = 0;
            var t = 0;
            var t_el = document.getElementById("el_type" + id).textContent;
            if (r != null) {
                if (t_el != "quete" || "enigme") {
                    if (Number(tb_ele.indexOf(1)) != -1 || Number(tb_ele.indexOf(18)) != -1 || Number(tb_ele.indexOf(21)) != -1 || Number(tb_ele.indexOf(22)) != -1) {
                        t--;
                    }
                    if (id == 2) {
                        t++;
                    }
                }
                r.forEach(element => {
                    if (Number(tb_ele.indexOf(element)) != -1) {
                        i++;
                    }
                });
                if (i == r.length && r.length == tb_ele.length + t) {
                    //Tous les objets sont là
                    if (t_el == "enigme") {
                        enigme(id);
                    } else {
                        utilisation(id);
                        free(id);
                    }
                } else {
                    //il manque des objets pour libérer
                }
            } else {
                //L'objet est libéré car rien ne le bloque
                free(id);
            }
        })

}

function free(id) {
    /*
    Fonction qui récupère les id de tous les objets libérés par le dévérouillage d'un objet
    :param id: id de l'objet dévérouillé
    :type id: int
    :return: list des id libérés par l'objet pour être affichés (par free)
    :rtype: array tableau associatif avec comme clé id de l'objet
    */
    var data = "tb_free=" + id;
    //Trouve la liste des objets qui vont devenir visible suite à l'ouverture de l'objet
    fetch("../php/bdd.php", {
            method: "post",
            body: data,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }

        })
        .then(r => r.json())
        .then(r => {
            var type = document.getElementById("el_type" + id).textContent;
            get_objet(getMark(id));
            if (r != null) {
                afficher(r);
            } else {
                if (type == "end") {
                    stop();
                }
            }
        });
}

//---enigme---
//paramètres
var popup_title = document.getElementById("titre");
var popup_contenu = document.getElementById("contenu");
var soluce = document.getElementById("soluce");
var popup_title2 = document.getElementById("titre2");
var popup_contenu2 = document.getElementById("contenu2");
soluce.style.display = "none";

//fonctions
function enigme(id) {
    /*
    Fonction récupérant l'énigme et la posant sous forme de pop-up
    :param el: element sous la forme d'un tableau associatif
    :type el: tableau associatif
    */
    popup_title.innerHTML = document.getElementById("el_nom" + id).textContent;
    popup_title2.innerHTML = document.getElementById("el_nom" + id).textContent;
    popup_contenu.innerHTML = document.getElementById("el_indice" + id).textContent;
    popup_contenu2.innerHTML = document.getElementById("el_resolu" + id).textContent;
    soluce.innerHTML = document.getElementById("el_soluce" + id).textContent;
    openForm(id);
}

function confirmer() {
    /*
    Fonction permettant de vérifier la réponse et déclencher la suite en cas de bonne réponse
    */
    var prop = document.getElementById("proposition");
    var proposition = prop.value;
    var sol = soluce.textContent;
    var el_id = Number(document.getElementById("el_id").textContent);
    if (proposition == sol) {
        prop.classList.remove("erreur");
        utilisation(el_id);
        free(el_id);
        closeForm();
        openResolve();
    } else {
        prop.classList.add("erreur");
        prop.value = "";
    }
}

function openResolve() {
    /*
    Fonction qui affiche la résolution de l'énigme
    */
    var close2 = document.getElementById("close2");
    document.getElementById("resolution").style.display = "block";
    close2.addEventListener('click', closeResolve);

}

function closeResolve() {
    /*
    Fonction qui ferme la résolution de l'énigme
    */
    var close2 = document.getElementById("close2");
    document.getElementById("resolution").style.display = "none";
    close2.removeEventListener('click', closeResolve);

}

function openForm(id) {
    /*
    Fonction ouvrant le Popup 
    */
    var prop = document.getElementById("proposition");
    prop.value = "";
    var confirm = document.getElementById("confirm");
    var close = document.getElementById("close");
    document.getElementById("myForm").style.display = "block";
    close.addEventListener('click', closeForm);
    var el_id = document.getElementById("el_id");
    el_id.innerHTML = id;
    confirm.addEventListener('click', confirmer);
}

function closeForm() {
    /*
    Fonction fermant le popup
    */
    var close = document.getElementById("close");
    var confirm = document.getElementById("confirm");
    document.getElementById("myForm").style.display = "none";
    close.removeEventListener('click', closeForm);
    confirm.removeEventListener('click', confirmer);
}
/*
----------------------------------------------------------------------------
------------------------------------Jeu-------------------------------------
----------------------------------------------------------------------------
*/

//début chrono
start();

//Map
var mymap = L.map('mapid', { minZoom: 1 }).setView([48.871953, 2.331805], 20);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);
mymap.on('zoomend', function() { //gestion zoom
    majMark();
});

//paramètres
var inventaire = [];
var visible = [1, 21, 22];
var selection = [];
var markers = [];

//affichage initial
getById();
setTimeout(function() { afficher(visible); }, 100);