var banniere = document.getElementById("banniere");

function findplayer() {
    var data = "best_player=1";
    fetch("./php/bdd.php", {
            method: "post",
            body: data,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            }
        })
        .then(r => r.text())
        .then(r => {
            banniere.innerHTML = r;
        })
}
window.addEventListener("load", function() {
    function defile() {
        var texte = banniere.firstElementChild;
        var tailleTexte = banniere.scrollWidth;
        if (texte != null) {
            var pos = texte.style.marginLeft.replace("px", "")
            pos -= 10;
            if (pos < -tailleTexte - 100) {
                pos = 200
            }
            texte.style.marginLeft = pos + "px";
            setTimeout(defile, 100);
        }
    }
    findplayer();
    setTimeout(defile, 100);
});