<?php

//Conexion à la base de données
include("connect.php");

//Selection par id
if(isset($_POST["id"])){
  $sql = "SELECT * FROM tbl_objets";
  $result = mysqli_query($link, $sql);
  if($result){
		while($ligne=mysqli_fetch_assoc($result)){
      $array[$ligne["id"]] = $ligne;
    }
    echo json_encode($array, JSON_NUMERIC_CHECK);
  }
}

//Liste des objets qui liberent un objet
elseif(isset($_POST["tb_lock"])){
  $sql = "SELECT id FROM tbl_objets WHERE libere=".$_POST["tb_lock"];
  $result = mysqli_query($link, $sql);
  if($result){
		while($ligne=mysqli_fetch_assoc($result)){
      $array[] = $ligne["id"];
    }
    echo json_encode($array, JSON_NUMERIC_CHECK);
  }
}

//Liste des objets liberes par un objet
elseif(isset($_POST["tb_free"])){
  $sql = "SELECT id FROM tbl_objets WHERE bloque=".$_POST["tb_free"];
  $result = mysqli_query($link, $sql);
  if($result){
		while($ligne=mysqli_fetch_assoc($result)){
      $array[] = $ligne["id"];
    }
    echo json_encode($array, JSON_NUMERIC_CHECK);
  }
}

//Liste des joueurs
elseif(isset($_POST["tb_player"])){
  if($_POST["tb_player"]=="rank"){
    $sql = "SELECT * FROM tbl_players ORDER BY rang";
  }
  elseif($_POST["tb_player"]=="names"){
    $sql = "SELECT * FROM tbl_players ORDER BY nom";
  }
  else{
     $sql ="SELECT * FROM tbl_players WHERE nom LIKE '%".$_POST['tb_player']."%'";
  }
  $result = mysqli_query($link, $sql);
  $head = "<tr><th>Rang</th><th>Nom</th><th>Temps</th></tr>";
  if($result){
		while($ligne=mysqli_fetch_assoc($result)){
      $head = $head."<tr>
              <td>".$ligne["rang"]."</td>
              <td>".$ligne["nom"]."</td>
              <td>".$ligne["temps"]."</td>
            </tr>";
  	}
  }
  echo $head;
}

elseif(isset($_POST["add_player"])){
  $sql = "SELECT MAX(rang) FROM tbl_players";
  $result = mysqli_query($link, $sql);
  if($result){
		while($ligne=mysqli_fetch_assoc($result)){
      $rang = $ligne["MAX(rang)"] + 1;
    }
  $sql1 = "SELECT * FROM tbl_players WHERE temps > '".$_POST["temps"]."'";
  $result = mysqli_query($link, $sql1);
  if($result){
    $i=0;
		while($ligne=mysqli_fetch_assoc($result)){
      if($i == 0){
        $rang = $ligne["rang"];
      }
      $i++;
      $sql2 .= "UPDATE tbl_players SET rang = ".($ligne["rang"]+1)." WHERE id_player = ".$ligne["id_player"].";";  
    }
  }
  $sql2 .= "INSERT INTO tbl_players (rang, nom, temps) VALUES (".$rang.", '".$_POST["add_player"]."', '".$_POST["temps"]."')";
  if($conn->multi_query($sql2)){
    echo "yes";
  }
  else{
    echo "non";
  }
  }
}

elseif(isset($_POST["best_player"])){
  $sql = "SELECT * FROM tbl_players ORDER BY rang LIMIT 1";
  $result = mysqli_query($link, $sql);
  if($result){
    $head = "<p>Meilleur joueur:";
		while($ligne=mysqli_fetch_assoc($result)){
      $head = $head." ".$ligne["nom"]." en un temps de ".$ligne["temps"];
    }
    
  }
  $head = $head."</p>";
    echo $head;
}
?>